'use strict';
//he ngSanitize module provides functionality to sanitize HTML
var myApp = angular.module('boostrapTwitter3App', []);

//*******************
//Create Controller good manner 
//*******************

myApp.controller('VirtualCtrl', function ($scope) {
    
    //dummy data
    $scope.myTime = [
        {
            lap: 1,
            currentTime: 1,
            currentTimeUnix: 0,
            totalTime:0,
            totalTimeUnix:0
        }
    ];
});


//*******************
//Create filters
//*******************
myApp.filter('reverse', function(){
    return function (text){
        return text.split("").reverse().join("");
    }
})

//*******************
//Create Directive
//*******************
myApp.directive('stopwatch', function() {
    return {
        restrict: 'E',
        scope: {},//for isolation
        controller: function($scope, $timeout) {
            $scope.myTime = [];
			$scope.formatTime = "00<sup>h</sup>:00<sup>m</sup>:00<sup>s</sup>:000";
			$scope.Start ="Start";

            console.log($scope.myTime);
            //object timer
            var clsStopwatch = function() {
            
                // Private vars
                var startAt = 0,  // Time of last start / resume. (0 if not running)
                    lapTime = 0;  // Time on the clock when last stopped in milliseconds

                var now = function() {
                    return (new Date()).getTime();
                };

                // Start or resume
                this.start = function() {
                    startAt = startAt ? startAt : now();
                };

                // Stop or pause
                this.stop = function() {
                
                    // If running, update elapsed time otherwise keep it
                    lapTime = startAt ? lapTime + now() - startAt : lapTime;
                    startAt = 0; // Paused

                    return lapTime;
                };

                // Reset
                this.reset = function() {
                    lapTime = startAt = 0;
                };

                // Duration
                this.time = function() {
                    return lapTime + (startAt ? now() - startAt : 0);
                };
            };

            var x = new clsStopwatch();
            var clockTimer, timeoutId, isClick = false;

            //*******************
            //Create private methods
            //*******************
            var pad = function(num, size) {
                var s = "0000" + num;
                return s.substr(s.length - size);
            }

            var formatTime = function(time) {
                var h,m,s,ms;
                h = m = s = ms = 0;
                var newTime = '';

                h = Math.floor( time / (60 * 60 * 1000) );
                time = time % (60 * 60 * 1000);
                m = Math.floor( time / (60 * 1000) );
                time = time % (60 * 1000);
                s = Math.floor( time / 1000 );
                ms = time % 1000;

                newTime = pad(h, 2) + '<sup>h</sup>:' + pad(m, 2) + '<sup>m</sup>:' + pad(s, 2) + '<sup>s</sup>:' + pad(ms, 3);
                return newTime;
            }

            var formatTimeSample = function(time) {
            	var h,m,s,ms;
            	h = m = s = ms = 0;
            	var newTime = '';

            	h = Math.floor( time / (60 * 60 * 1000) );
            	time = time % (60 * 60 * 1000);
            	m = Math.floor( time / (60 * 1000) );
            	time = time % (60 * 1000);
            	s = Math.floor( time / 1000 );
            	ms = time % 1000;

            	newTime = pad(h, 2) + ':' + pad(m, 2) + ':' + pad(s, 2) + ':' + pad(ms, 3);
            	return newTime;
            }
            //*******************
            //Create public methods
            //*******************
            $scope.updateCronos = function() {
                $scope.formatTime =  formatTime(x.time());
            }

            $scope.startCronos = function () {
                console.log($scope.myTime);
                var arrLength, previousDate = 0, previosCount = 0;

                x.start();

                
                $scope.init();

                console.log("Click: " + isClick);

                if (isClick) {
                    //update JSON date time 
                    //count lenght
                    arrLength = $scope.myTime.length;

                    var myLap  = x.stop();

                    //convert to unix time
                    // console.log(arrLength);

                    if(arrLength){
                        previousDate = (new Date($scope.myTime[arrLength-1].currentTimeUnix)).getTime();
                        previosCount = $scope.myTime[arrLength-1].lap;
                    } 

                    $scope.myTime.push({
                       lap: (previosCount) +1,
                       currentTime: formatTimeSample(myLap - (previousDate)),
                       currentTimeUnix: myLap,
                       totalTime: formatTimeSample(myLap),
                       totalTimeUnix: myLap
                    });


                } 
				//update button
                $scope.Start = (isClick)? 'Start' :'Resume';
                
                //create toggle
                isClick = (!isClick) ? true: false;
                
            }

            // schedule update in one second
            $scope.init = function () {
                // save the timeoutId for canceling
                timeoutId = $timeout(function() {
                    $scope.updateCronos(); // update DOM
                    $scope.init(); // schedule another update
                    // console.count('Time');
                }, 100);
            }

            //reset time
            $scope.stopCronos = function () {
                console.log('stop');

                isClick = false;
                x.reset();
                $scope.updateCronos();

                $timeout.cancel(timeoutId);
                //update JSON with empty data
                $scope.myTime = [];
            }

            $scope.resetCronos = function (){
                console.log('reset');
            }

            $scope.timeCronos = function (){
              console.log('time');
            }
        },

        //the directive is linked to the DOM
        link: function(scope, iElement, iAttrs) {
        // get weather details
        }
    }
});

// myApp.directive('stopwatch_desc', function(){
//     return {
//         restrict: 'a',
//         require: 'stopwatch', //talk with directive stopwatch
//         link: function(scope, element, attrs, stopwatchCtrl) {
//             console.log('init method');
//         }
//     }
// })





