'use strict';

var myApp = angular.module('boostrapTwitter3App', ['ngResource']);


//*******************
//Create Service
//*******************
myApp.factory('instagram', function($resource){
	return {
		listPopular: function(callback){
			// The ngResource module gives us the $resource service. It makes working with
			// AJAX easy. Here I am using the client_id of a test app. Replace it with yours.

			var api = $resource('https://api.instagram.com/v1/media/popular?client_id=:client_id&callback=JSON_CALLBACK',{
				client_id: '642176ece1e7445e99244cec26f4de1f'
			},{
				// This creates an action which we've chosen to name "fetch". It issues
				// an JSONP request to the URL of the resource. JSONP requires that the
				// callback=JSON_CALLBACK part is added to the URL.

				fetch:{method:'JSONP'}
			});

			api.fetch(function(response){

				//call the supplied callback function
				callback(response.data);
			});
		}
	};
});
//*******************
//Create Controller
//*******************
myApp.controller('searchMode', function($scope, instagram){
	$scope.title = 'Grid - List Example';

	$scope.layout = 'grid';
	$scope.pics = [];

    //** private 
    var removeClass = function(elem){
    	var temp = angular.element('.nav.nav-tabs '+ elem).removeClass("active");  	
    }

    instagram.listPopular(function(data){
    	$scope.pics = data;
    });


	//** public
    $scope.checkActive = function(nameClass){

    	var temp = angular.element('.'+ nameClass);

    	removeClass('li');
    	if(typeof temp === 'object'){
    		temp.parent().attr('class','active');
    		$scope.layout = nameClass;
    	};
    };
});

