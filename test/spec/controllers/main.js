'use strict';

describe('Controller: MainCtrl', function () {

  // load the controller's module
  beforeEach(module('boostrapTwitter3App'));

  var MainCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MainCtrl = $controller('MainCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});


describe('filter', function(){
  
  //load controller module
  beforeEach(module('boostrapTwitter3App'));
  
  describe('reverse', function(){
      it('should reverse a string', inject(function(reverseFilter){
        expect(reverseFilter('ABCD').toEqual('DCBA'));
      }))
  });
});
