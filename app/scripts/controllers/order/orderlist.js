'use strict';

var myApp = angular.module('boostrapTwitter3App', ['pascalprecht.translate']);

//*******************
//Create Controller good manner
//*******************
myApp.controller('OrderCtrl', function ($scope, $translate) {
	
	$scope.services = [
		{
			name: 'Web Development',
			price: 300,
			active:true
		},{
			name: 'Design',
			price: 400,
			active:false
		},{
			name: 'Integration',
			price: 250,
			active:false
		},{
			name: 'Training',
			price: 220,
			active:false
		}
	];
console.log($translate);
	$scope.totalPrice = function(){

        var total = 0;

        //update TOTAL based on the list JSON with condition true
        angular.forEach($scope.services, function(s){
        	if(s.active){ total += s.price;}
        })


        return total;
	};


	$scope.toggleProduct = function(e,s){
		//toggle variable  and show on clas
		s.active = !s.active;
        //stop click on browser
        e.preventDefault();
      
	}
});