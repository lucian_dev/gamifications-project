'use strict';

var myApp = angular.module('boostrapTwitter3App', ['ngResource','jmdobry.angular-cache']);

//*******************
//Create Services
//*******************
myApp.factory('DbInterface', function($resource){
    var apiDb = $resource('https://api.mongolab.com/api/1/databases' +
          '/dev_db/collections/todo_table/:id',
        { apiKey:'VgG7HUCHaEmmfRqc1woi0bQFcOZ04S8n' }, 
        { update: {method: 'PUT', headers: [{'Content-Type': 'application/json'}]} }
    );

    apiDb.prototype.update = function(cb) {
        console.log('>>update>>');

        if(typeof(cb) === 'undefined') return "error";
        return apiDb.update({id: cb._id.$oid}, angular.extend({}, cb, {_id:undefined}));
    };

    apiDb.prototype.destroy = function(cb) {
        console.log('delete');
        return apiDb.remove({id: cb._id.$oid});
    };

    return apiDb;
});


//Service Cache
//don`t have deep level to read.  need to extract $oid, and format JSOn from TO DO LIST 
myApp.factory('Cache',function($q, $http, $angularCacheFactory){

    var profileCache = $angularCacheFactory('dataCache', {
        // This cache can hold 1000 items
        capacity: 1000,
        maxAge: 3600000, // Items added to this cache expire after 1* 1000 seconds.
        // This cache will check for expired items every minute
        recycleFreq: 60000,
        // This cache will clear itself every hour
        cacheFlushInterval: 3600000,
        // Full synchronization with localStorage on every operation
        verifyIntegrity: true,
        deleteOnExpire: 'aggressive', // Items will be deleted from this cache right when they expire.
        storageMode: 'localStorage' // This cache will sync itself with `localStorage`.
    });

    return {
        list: 0,
        getDataById: function (id) { //not recommended
            var deferred = $q.defer(),
                start = new Date().getTime();

            $http.get('api/data/' + id, {
                cache: $angularCacheFactory.get('dataCache')
            }).success(function (data) {
                console.log('time taken for request: ' + (new Date().getTime() - start) + 'ms');
                deferred.resolved(data);
            });
            return deferred.promise;
        },

        info: function(){
            return profileCache.info();
        },

        put: function(key, value){
            // console.log('************');
            // console.log(value);
            profileCache.put('/tasks/1', {
                key: value
            });
        },

        get currList(){
            if (typeof(this.list) === 'object') return this.list.key;
            return 0;
        },

        set currList(id){
            this.list = profileCache.get('/tasks/'+ id);
        }
    }
});


//*******************
//Create Controller
//*******************
myApp.controller('TodoCtrl', function ($scope, DbInterface, Cache) {

    $scope.addTodo = function() {
        $scope.error = false;
        
        if($scope.todoText.length > 2)
        {
            console.log('Save DB');
            //save to DB
            var tmpData = {};
            var tmpData = new DbInterface();
            tmpData.text = $scope.todoText;
            tmpData.active = false;
            //puts the object on your server
            tmpData.$save(); 

            $scope.todos  = $scope.getListData();

            //cache data
            Cache.put('items',$scope.todos);
            console.log(Cache.info());
        } else {
            $scope.error = true;
        }
        $scope.todoText = '';
    };

    $scope.remaining = function() {
        var count = 0;
        angular.forEach($scope.todos, function(todo) {
          count += todo.active ? 0 : 1;
      });

        return count;
    };

    $scope.archive = function() {
        $scope.error = false;
        var oldTodos = $scope.todos;
        $scope.todos = [];

        angular.forEach(oldTodos, function(todo) {
          if (!todo.active) $scope.todos.push(todo);
          else {
            //destroy db
            console.log('Delete from DB');
            var clearData = new DbInterface();
            clearData.destroy(todo);
          }
      });
    };
   
    $scope.checkTodo = function(todo) {
        todo.active = !todo.active;

        //update cache
        // console.log('Update cache');
        Cache.currList = '1';
        //update db        
        console.log('Update data');
        setInterval(function(){
            var updateData = new DbInterface();
            updateData.update(todo);
        }, 120* 1000);
    };

    $scope.readFromCache = function(key){
        console.log('Obj Cache');
        // Cache.currList = '1'; // set cache to be with ID 1
        // console.log(Cache);
        return Cache.currList;
    }

    $scope.getCacheStats = function(){

        return Cache.info();
    }

    $scope.getListData = function(){
        // var tmpData = $scope.readFromCache();
        // console.log('/////////////');
        // console.log(tmpData);
        // if(tmpData !== 0) return tmpData;
        return DbInterface.query();
    }

    $scope.errorMsg = 'At least 3 characters required!!';
    $scope.title = 'Tutorial Todo List';
    $scope.todoText = '';

    $scope.todos  = $scope.getListData();
});