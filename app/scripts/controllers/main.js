'use strict';

var myApp = angular.module('boostrapTwitter3App', []);


//*******************
//Create Controller good manner 
//*******************
myApp.controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma',
      'Boostrap 3',
      'Font Awesome'
    ];

    $scope.myFunction = function() {
        alert('Loading myFunction');
    }



  });


myApp.controller('VirtualCtrl', function ($scope) {
	$scope.logChore = function (chore){
		alert( chore + " is done");
	}

	$scope.ctrlFlavor = 'blackberry';


	$scope.callHome = function (message) {
		alert(message)
	}

	this.sayHy = function() {
		alert('hello function');
	}

	return $scope.VirtualCtrl =this;
});


//*******************
//create a service
//*******************
myApp.factory('Data', function(){
	return {message:"I`m data from service"}
})

myApp.factory('Avengers', function(){
	var Avengers = [];
    Avengers.cast = [
        {
            name: "Robert Downey Jr.",
            character: "Tony Stark / Iron Man"
        },
        {
            name: "Chris Evans",
            character: "Steve Rogers / Captain America}"
        },
        {
            name: "Mark Ruffalo",
            character: "Bruce Banner / The Hulk}"
        },
        {
            name: "Chris Hemsworth",
            character: "Thor"
        },
        {
            name: "Scarlett Johansson",
            character: "Natasha Romanoff / Black Widow"
        },
        {
            name: "Jeremy Renner",
            character: "Clint Barton / Hawkeye"
        },
        {
            name: "Tom Hiddleston",
            character: "Loki"
        },
        {
            name: "Clark Gregg",
            character: "Agent Phil Coulson"
        },
        {
            name: "Cobie Smulders",
            character: "Agent Maria Hill"
        }
    ];
	return Avengers;
})


//create filter
myApp.filter('reverse', function(){
	return function (text){
		return text.split("").reverse().join("");
	}
})



// myApp.filter('reverse', function(){
// 	return function (){
// 		return '';
// 	}
// })

//*******************
//create directive
//*******************
myApp.directive('superman', function(){
	return {
		// restrict: "E", //restrict for elements tag
		restrict: "A", //restrict for attributes
		// restrict: "C", //restrict class
		// template: "<div>Here I am to save the day</div>"
		link: function() {
			// alert("Work it strong!");
		}
	}
})



myApp.directive('superhero', function(){
    return {
        restrict: "E", //restrict for elements tag
        scope:{}, //isolated
        controller: function($scope) {
            $scope.abilities = [];
            this.addStrenght = function(){
                $scope.abilities.push("strenght");
            }

            this.addSpeed = function(){
                $scope.abilities.push("speed");
            }

            this.addFlight = function(){
                $scope.abilities.push("flight");
            }
        },

        link: function(scope, element) {
            element.addClass("btn-block");
            element.bind("mouseenter", function(){
                console.log(scope.abilities);
            })
        }
    }
})

myApp.directive('strenght', function(){
    return {
        require: 'superhero', //talk with directive superhero
        link: function(scope, element, attrs, superheroCtrl) {
            superheroCtrl.addStrenght();
        }
    }
})

myApp.directive('speed', function(){
    return {
        require: 'superhero', //talk with directive superhero
        link: function(scope, element, attrs, superheroCtrl) {
            superheroCtrl.addSpeed();
        }
    }
})


myApp.directive('flight', function(){
    return {
        require: 'superhero', //talk with directive superhero
        link: function(scope, element, attrs, superheroCtrl) {
            superheroCtrl.addFlight();
        }
    }
})



myApp.directive('flash', function(){
	return {
		restrict: "A", //restrict for attributes
		link: function() {
			// alert("Work it faster!");
		}
	}
})

myApp.directive('enter', function () {
	return function (scope, element, attrs){
		element.bind('mouseenter',function(){
			//element.addClass(attrs.enter);
            scope.$apply(attrs.enter);
		})
	};
})


myApp.directive('leave', function () {
	return function (scope, element, attrs){
		element.bind('mouseleave',function(){
			element.removeClass(attrs.enter);
		})
	};
})


myApp.directive('kid', function () {
	return {
		restrict: 'E',
		scope: {
			done: '&'//map like an expression 
		},//isolation
		template: '<input type="text" ng-model="chore" class="form-control input-lg" />{{chore}}' +
		  '<button class="button" ng-click="done({chore:chore})">I\'am done</button>'
		  //{chore:chore} -- passing expresion with parameters
	};
})

myApp.directive('drink', function () {
	return {
		scope: {
			//flavor: "@" //same like method link with assign  scope.flavor = attrs.flavor, expect a string
			flavor: '=' //expect to have
		}, //isolate
		template: '<div>{{flavor}}</div>',

	};
})

//We have a model (ng-model), a function (sendMail()), and a string (from-name). To get access to these on your directive’s scope:
// scope: {
//   ngModel: '=',     // Bind the ngModel to the object given
//   onSend: '&',      // Pass a reference to the method 
//   fromName: '@'     // Store the string associated by fromName
// }
myApp.directive('phone', function () {
	return {
		// by default is restrict set to A => attributes
		scope: {
			dial: '&' //map an expression an invoke from controller
		}, //isolate

		template: '<input type="text" ng-model="value" />' +
		'<button class="btn" ng-click="dial({message:value})">Call home!</div>',

	};
})

myApp.directive('panel', function(){
	// Runs during compile
	return {
		restrict: "E",
		transclude: true,//use this config to append html content from index HTML page
		template: '<div class="panel" ng-transclude>This is a panel component</div>'
	};
});


//*******************
//create Controllers old WAY
//*******************
function FirstCtrl($scope, Data){
  $scope.data = Data;
}

function SecondCtrl ($scope, Data) {
	$scope.data = Data;

	$scope.reversedMessage = function (message) {
		return message.split("").reverse().join("");
	}
}

function AvengersCtrl($scope, Avengers) {
	$scope.avengers = Avengers;
}


console.log(myApp);