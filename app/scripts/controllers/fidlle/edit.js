'use strict';

var myApp = angular.module('boostrapTwitter3App', []);


//*******************
//Create Controller good manner 
//*******************
myApp.controller('inlineEditorCtrl', function ($scope) {


	// $scope is a special object that makes
	// its properties available to the view as
	// variables. Here we set some default values:
	$scope.title = "Tooltip example";
	$scope.isShow = false;

	// Some helper functions that will be
	// available in the angular declarations

	$scope.hideTooltip = function(){

		// When a model is changed, the view will be automatically
		// updated by by AngularJS. In this case it will hide the tooltip.
		$scope.isShow = false;
	}

	$scope.toggleTooltip = function(e){
		e.stopPropagation();
		$scope.isShow = !$scope.isShow;
	}
});