'use strict';

var myApp = angular.module('boostrapTwitter3App', []);

//*******************
//Create service
//*******************
myApp.factory('missions', function($rootScope) {
     //create list objects mission
    var arrMission3 = [
        {
            status: false,
            title: 'Baga bani'
        },
        {
            status: false,
            title: 'Baga bani'
        },
        {
            status: false,
            title: 'Baga bani'
        },
        {
            status: false,
            title: 'Baga bani'
        }
    ];

    var arrMission = [
        {
            status: false,
            title: 'Organize your files'
        },
        {
            status: false,
            title: 'Name your files'
        },
        {
            status: false,
            title: 'Lazy-load your code'
        },
        {
            status: false,
            title: 'Serialize application state to the URL'
        },
        {
            status: true,
            title: 'Derive application state from the URL'
        }
    ];

    var arrMission2 = [
        {
            status: false,
            title: 'Manage a Model Layer'
        },
        {
            status: false,
            title: 'Do smart caching'
        },
        {
            status: false,
            title: 'Work with a RESTful interface (for real)'
        },
        {
            status: false,
            title: 'Use a 3rd-party lib with Angular'
        }
    ];

    //create init list with objects
    var triggerMissions = [
        { lvl: 2, list: arrMission  },
        { lvl: 3, list: arrMission2 },
        { lvl: 4, list: arrMission  },
        { lvl: 5, list: arrMission2 },
    ];
    
    return {
        reset:[{
            status: true,
            title: 'FREE MODE'
        }],
        now: 0,
        resetNow: function(){
            this.now = this.reset;
        },
        current: triggerMissions,
        fetchAll : function(){
            return this.triggerMissions
        },

        currMission: function(level){
            if(!angular.isNumber(level)) return 0;

            for (var i=0, max = this.current.length; i < max; i = i+1) {
                if(level === this.current[i].lvl) 
                {

                    this.now = this.current[i].list;
                    console.log(this.now);
                    return this.now;
                }
                this.now = this.reset;
            }  
        }
    };
});

//*******************
//Create Controller
//*******************
myApp.controller('gamificationCtrl', function ($scope, missions) {

    var rangArr = ['novice', 'advanced beginner', 'competent', 'proficient', 'expert'];
    var lvlArr = [0,10,15,20,30,40,70,100];
    // var lvlArr = [0,2,3,4,5,6,7,100];
    var listTrophy = [];
    var imgUrl = 'images/trophy/';
    var trophyArr = [
        {
            lvl: lvlArr[1],
            trophy: "55_3.gif",
            title: "Novice 1"
        },
        {
            lvl: lvlArr[2],
            trophy: "68_3.gif",
            title: "Novice 2"
        },
        {
            lvl: lvlArr[3],
            trophy: "77_3.gif",
            title: "Novice 3"
        },
        {
            lvl: lvlArr[4],
            trophy: "91_3.gif",
            title: "Novice 3"
        },
        {
            lvl: lvlArr[5],
            trophy: "372_3.gif",
            title: "Novice 1"
        },
        {
            lvl: lvlArr[6],
            trophy: "109_3.gif",
            title: "Novice 2"
        }
    ]
    var currScore = 500;

    $scope.title = 'Tutorial Gamification Example';
    $scope.hero = {
        level: 1,
        progress: 80,
        rang: rangArr[0],
        trophies: [],
        score: currScore
    };
   
    
    angular.element('#level-progress').css({width: $scope.hero.progress + "%"});

    $scope.add = function(){
        var hero = $scope.hero;
        var progress = hero.progress;
        var prevProgres = progress;
        
        //update score per click
        $scope.hero.score = $scope.hero.score + 10;
        
        //check progress is full
        progress = (progress >= 100)? 0 :(progress + 10);
        
        angular.element('#level-progress').css({width: progress + "%"});

        

        if(prevProgres >= 100) { 
            $scope.hero.level = $scope.hero.level + 1; 
            //update score
            $scope.hero.score = $scope.hero.score + 50;

            //add sound when level up
            var snd = new Audio("media/skyrim_level_up.mp3"); // buffers automatically when created
            snd.play();

            //update new missions
            //activate handle
            missions.currMission(hero.level);
        }

        //give trophies
        $scope.hero.progress = progress;

        // console.log("Rang Cache: " + hero.level);

        //check level
        var iLvl = 0;
        for (var i=0, max = lvlArr.length; i < max; i = i+1) {
            if(hero.level < lvlArr[i]) 
            {
                iLvl = i-1;
                // console.log(hero.level);
                // console.log(iLvl);
                break;
            }
        };

        // console.log("hero lvl:" + hero.level);
        // console.log("index lvl:" + iLvl);

        //increase rang
        if((hero.level == lvlArr[iLvl]) && (progress == 0)){
            var index = 0, 
            listTrophy = hero.trophies;
            //add trophy
            trophyArr.forEach(function(obj, key) {
                // console.log("trophy Level: " + obj.lvl);
                if(obj.lvl === hero.level)  listTrophy.push({href: (imgUrl + obj.trophy), title: obj.title});
            });
            //next rang
            rangArr.forEach(function(value, key) {
                if(value === hero.rang) index = key + 1;
            });

            // console.log(index);
            // console.log(listTrophy);
            hero.rang = (typeof rangArr[index] != 'undefined')? rangArr[index] : rangArr[rangArr.length - 1];
        }

        // console.log($scope.hero);
    },

    $scope.checkTrophies = function(){
        console.log($scope.hero.trophies.length);
        if(typeof($scope.hero.trophies) === 'object' && $scope.hero.trophies.length > 0)  return true;
        return false;
    }
});


//here goals
myApp.controller('missionCtrl', function ($scope, missions) {

    //default  mission
    missions.resetNow();
    $scope.currMission = missions;
    console.log(missions.now);
});
