'use strict';

var myApp = angular.module('boostrapTwitter3App', []);


//*******************
//Create Controller
//*******************
myApp.controller('MenuVirtualCtrl', function ($scope) {

  $scope.title = 'Tutorial Navigation Bar';
  $scope.show = '';

  var removeClass = function(elem){
	var temp = angular.element('.nav.nav-tabs '+ elem).removeClass("active");  	
  }

  $scope.checkActive = function(nameClass){
 	
 	var temp = angular.element('.'+ nameClass);
	
	removeClass('li');
 	if(typeof temp === 'object'){
 		temp.parent().attr('class','active');
		$scope.show = nameClass;
 	};
  };
});