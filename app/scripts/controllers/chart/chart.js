'use strict'

//replace $.jqplot with jQuery.jqplot
var myApp = angular.module('myChartingApp', ['ui.chart']);

 
myApp.value('charting', {
  pieChartOptions: {
    seriesDefaults: {
          // Make this a pie chart.
          renderer: jQuery.jqplot.PieRenderer,
          rendererOptions: {
            // Put data labels on the pie slices.
            // By default, labels show the percentage of the slice.
            showDataLabels: true
        }
    },
    legend: { show:true, location: 'e' }
  },
  barChartOptions: {
        seriesDefaults:{
            renderer: jQuery.jqplot.BarRenderer,
            rendererOptions: {fillToZero: true}
        },
        // Custom labels for the series are specified with the "label"
        // option on the series option.  Here a series option object
        // is specified for each series.
        series:[
            {label:'Bar legend'}
        ],
        // Show the legend and put it outside the grid, but inside the
        // plot container, shrinking the grid to accomodate the legend.
        // A value of "outside" would not shrink the grid and allow
        // the legend to overflow the container.
        legend: {
            show: true,
            placement: 'outsideGrid'
        },
        axes: {
            // Use a category axis on the x axis and use our custom ticks.
            xaxis: {
                renderer: jQuery.jqplot.CategoryAxisRenderer,
                ticks: []
            },
            // Pad the y axis just a little so bars can get close to, but
            // not touch, the grid boundaries.  1.2 is the default padding.
            yaxis: {
                pad: 1.05,
                
            }
        }
  }
})

////*******************
//Create Controller 
//*******************
 myApp.controller('DemoPieCtrl', function ($scope, charting) {
    $scope.someData = [[
        ['Heavy Industry', 12],['Retail', 9], ['Light Industry', 14],
        ['Out of home', 16],['Commuting', 7], ['Orientation', 9]
    ]];

    $scope.myChartOpts = charting.pieChartOptions;
});

myApp.controller('DemoBarCtrl', function ($scope, charting) {
    var s1 = [200, 600, -700, 1000, 300, -100, 250];
    var ticks = ['May', 'June', 'July', 'August'];
    
    charting.barChartOptions.axes.xaxis.ticks = ticks;

    $scope.someData1 = [s1];
    
 
    $scope.myChartOpts1 = charting.barChartOptions;
});